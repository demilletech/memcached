/* -*- Mode: C; tab-width: 4; c-basic-offset: 4; indent-tabs-mode: nil -*- */

#include "memcached.h"
#include "jenkins_hash.h"
#include "murmur3_hash.h"
#define XXH_INLINE_ALL
//#define XXH_STATIC_LINKING_ONLY
#define XXH_FORCE_MEMORY_ACCESS 1
#define XXH_FORCE_ALIGN_CHECK 0
#define XXH_ACCEPT_NULL_INPUT_POINTER 1
#include "xxhash.h"
#include "xxh3.h"

int hash_init(enum hashfunc_type type)
{
    switch (type)
    {
    case JENKINS_HASH:
        hash = jenkins_hash;
        settings.hash_algorithm = "jenkins";
        break;
    case MURMUR3_HASH:
        hash = MurmurHash3_x86_32;
        settings.hash_algorithm = "murmur3";
        break;
    case XX_HASH:
        hash = XXH_wrapper;
        settings.hash_algorithm = "xxhash";
        break;
    case XXH3_HASH:
        hash = XXH3_wrapper;
        settings.hash_algorithm = "xxh3";
        break;
    default:
        return -1;
    }
    return 0;
}
