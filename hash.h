#ifndef HASH_H
#define HASH_H

typedef uint32_t (*hash_func)(const void *key, size_t length);
hash_func hash;

enum hashfunc_type
{
    JENKINS_HASH = 0,
    MURMUR3_HASH = 1,
    XX_HASH = 2,
    XXH3_HASH = 3
};

int hash_init(enum hashfunc_type type);

#endif /* HASH_H */
